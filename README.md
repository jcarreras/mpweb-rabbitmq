# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.gif)

# Descripción
-----------------------

Funcionamiento básico del negociador de mensajes RabbitMQ

Ejemplo de un productor de mensajes y un consumidor de los mismos.


# Instalación
-----------------------

```
$ make up

```


# Instrucciones
-----------------------

- Lee el contenido de los dos ficheros: `producer.php` y `worker.php`
- Entra en un container con `make bash`
- Ejecuta `php producer.php`, para encolar un mensaje
- Entra en otro un container con `make bash`
- Ejecuta `php worker.php` para dejar encendido el consumidor de tareas. Debería haber consumido la que acabamos de crear.
- Prueba de ejecutar repetidas veces el productor de mensajes para ver que se consumen por el worker.


# Posibles problemas

Si no puedes conectar Rabbit, espera unos minutos que el servicio tarda un poco en arrancar, e intentalo de nuevo.


```
Fatal error: Uncaught PhpAmqpLib\Exception\AMQPIOException: stream_socket_client(): unable to connect to tcp://rabbit:5672 (Connection refused) in /app/vendor/php-amqplib/php-amqplib/PhpAmqpLib/Wire/IO/StreamIO.php:109
   Stack trace:
   #0 /app/vendor/php-amqplib/php-amqplib/PhpAmqpLib/Connection/AbstractConnection.php(219): PhpAmqpLib\Wire\IO\StreamIO->connect()
   #1 /app/vendor/php-amqplib/php-amqplib/PhpAmqpLib/Connection/AbstractConnection.php(203): PhpAmqpLib\Connection\AbstractConnection->connect()
   #2 /app/vendor/php-amqplib/php-amqplib/PhpAmqpLib/Connection/AMQPStreamConnection.php(69): PhpAmqpLib\Connection\AbstractConnection->__construct('guest', 'guest', '/', false, 'AMQPLAIN', NULL, 'en_US', Object(PhpAmqpLib\Wire\IO\StreamIO), 0, 3, 0)
   #3 /app/producer.php(9): PhpAmqpLib\Connection\AMQPStreamConnection->__construct('rabbit', 5672, 'guest', 'guest')
   #4 {main}
     thrown in /app/vendor/php-amqplib/php-amqplib/PhpAmqpLib/Wire/IO/StreamIO.php on line 109
```


# Desinstalación
-----------------------

```
$ make remove
```
