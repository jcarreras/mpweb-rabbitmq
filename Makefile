

up: build composer
	docker-compose up -d

composer:
	docker-compose run --rm composer install --ignore-platform-reqs

build:
	docker-compose build

down:
	docker-compose down

bash:
	docker-compose run --rm php bash

remove:
	docker-compose rm -f